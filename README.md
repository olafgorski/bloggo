# Bloggo
Bloggo is a very simple blogging platform written in Flask, Python3.

Some things are finished, some are not.

What is done:

1. adding news/drafts and publishing em
2. editing entries
3. tags and searching by em
4. full-text search
5. giving 'kudos' to each post - 1 per/ip (dk if works properly on normal server, on localhost it works)
6. comments
7. recaptcha
8. contact form
9. atom feed


# Installation
Easy.
Just type this into your console

```
pip install -r requirements.txt

python app.py
```
