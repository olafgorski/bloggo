import datetime, functools, os, re, urllib.parse
import smtplib

from urllib.parse import urljoin
from werkzeug.contrib.atom import AtomFeed
from werkzeug.security import generate_password_hash, check_password_hash
from flask import (Flask, flash, Markup, redirect, render_template, request,
                   Response, session, url_for)
from markdown import markdown
from markdown.extensions.codehilite import CodeHiliteExtension
from markdown.extensions.extra import ExtraExtension
from micawber import bootstrap_basic, parse_html
from micawber.cache import Cache as OEmbedCache
from peewee import *
from playhouse.flask_utils import FlaskDB, get_object_or_404, object_list
from playhouse.sqlite_ext import *
from flask_recaptcha import ReCaptcha


ADMIN_PASSWORD = 'secret'
APP_DIR = os.path.dirname(os.path.realpath(__file__))

DATABASE = 'sqliteext:///%s' % os.path.join(APP_DIR, 'blog.db')
DEBUG = False

ADMIN_EMAIL = ''
ADMIN_SMTP_EMAIL = ''
ADMIN_SMTP_EMAIL_PASSWORD = ''
SMTP_SERVER = ''
SMTP_PORT = 0

RECAPTCHA_ENABLED = True
RECAPTCHA_SITE_KEY = ''
RECAPTCHA_SECRET_KEY = ''

SECRET_KEY = 'shhh, secret!'

SITE_WIDTH = 800

app = Flask(__name__)
app.config.from_object(__name__)

recaptcha = ReCaptcha()
recaptcha.init_app(app)

flask_db = FlaskDB(app)

database = flask_db.database

oembed_providers = bootstrap_basic(OEmbedCache())


class Entry(flask_db.Model):
    title = CharField()
    slug = CharField(unique=True)
    content = TextField()
    published = BooleanField(index=True)
    timestamp = DateTimeField(default=datetime.datetime.now, index=True)
    tags = CharField(default="", index=True)

    @property
    def html_content(self):
        hilite = CodeHiliteExtension(linenums=False, css_class='highlight')
        extras = ExtraExtension()
        markdown_content = markdown(self.content, extensions=[hilite, extras])
        oembed_content = parse_html(
            markdown_content,
            oembed_providers,
            urlize_all=True,
            maxwidth=app.config['SITE_WIDTH'])
        return Markup(oembed_content)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = re.sub('[^\w]+', '-', self.title.lower()).strip('-')
        ret = super(Entry, self).save(*args, **kwargs)
        self.update_search_index()
        return ret

    def update_search_index(self):
        query = (FTSEntry
                 .select(FTSEntry.docid, FTSEntry.entry_id)
                 .where(FTSEntry.entry_id == self.id))
        try:
            fts_entry = query.get()
        except FTSEntry.DoesNotExist:
            fts_entry = FTSEntry(entry_id=self.id)
            force_insert = True
        else:
            force_insert = False
        fts_entry.content = '\n'.join((self.title, self.content))
        fts_entry.save(force_insert=force_insert)

    @classmethod
    def public(cls):
        return Entry.select().where(Entry.published == True)

    @classmethod
    def drafts(cls):
        return Entry.select().where(Entry.published == False)

    @classmethod
    def search(cls, query):
        words = [word.strip() for word in query.split() if word.strip()]
        if not words:
            return Entry.select().where(Entry.id == 0)
        else:
            search = ' '.join(words)
        return (FTSEntry
                .select(
                    FTSEntry,
                    Entry,
                    FTSEntry.rank().alias('score'))
                .join(Entry, on=(FTSEntry.entry_id == Entry.id).alias('entry'))
                .where(
                    (Entry.published == True) &
                    (FTSEntry.match(search)))
                .order_by(SQL('score').desc()))


class Comment(flask_db.Model):
    entry = ForeignKeyField(Entry)
    name = CharField()
    email = CharField(index=True)
    content = TextField()
    timestamp = DateTimeField(default=datetime.datetime.now, index=True)

    class Meta:
        database = database


class Kudos(flask_db.Model):
    entry = ForeignKeyField(Entry)
    ip = CharField()


class FTSEntry(FTSModel):
    entry_id = IntegerField(Entry)
    content = TextField()

    class Meta:
        database = database


def login_required(fn):
    @functools.wraps(fn)
    def inner(*args, **kwargs):
        if session.get('logged_in'):
            return fn(*args, **kwargs)
        return redirect(url_for('login', next=request.path))
    return inner

@app.route('/login/', methods=['GET', 'POST'])
def login():
    next_url = request.args.get('next') or request.form.get('next')
    if request.method == 'POST' and request.form.get('password'):
        password = request.form.get('password')
        if password == app.config['ADMIN_PASSWORD']:
            session['logged_in'] = True
            session.permanent = True
            flash('You are now logged in.', 'success')
            return redirect(next_url or url_for('index'))
        else:
            flash('Incorrect password.', 'danger')
    return render_template('login.html', next_url=next_url)


@app.route('/logout/', methods=['GET', 'POST'])
def logout():
    if request.method == 'POST':
        session.clear()
        return redirect(url_for('login'))
    return render_template('logout.html')


@app.route('/')
def index():
    search_query = request.args.get('q')
    if search_query:
        query = Entry.search(search_query)
    else:
        query = Entry.public().order_by(Entry.timestamp.desc())
    return object_list(
        'index.html',
        query,
        search=search_query,
        check_bounds=False)


@app.route('/create/', methods=['GET', 'POST'])
@login_required
def create():
    if request.method == 'POST':
        if request.form.get('title') and request.form.get('content'):
            entry = Entry.create(
                title=request.form['title'],
                content=request.form['content'],
                published=request.form.get('published') or False,
                tags=request.form['tags']
            )
            flash('Entry created successfully.', 'success')
            if entry.published:
                return redirect(url_for('detail', slug=entry.slug))
            else:
                return redirect(url_for('edit', slug=entry.slug))
        else:
            flash('Title and Content are required.', 'danger')
    return render_template('create.html', entry=[])


@app.route('/<slug>/edit/', methods=['GET', 'POST'])
@login_required
def edit(slug):
    entry = get_object_or_404(Entry, Entry.slug == slug)
    if request.method == 'POST':
        if request.form.get('title') and request.form.get('content'):
            entry.title = request.form['title']
            entry.content = request.form['content']
            entry.published = request.form.get('published') or False
            entry.tags = request.form['tags']
            entry.save()

            flash('Entry saved successfully.', 'success')
            if entry.published:
                return redirect(url_for('detail', slug=entry.slug))
            else:
                return redirect(url_for('edit', slug=entry.slug))
        else:
            flash('Title and Content are required.', 'danger')

    return render_template('edit.html', entry=entry)


@app.route('/drafts/')
@login_required
def drafts():
    query = Entry.drafts().order_by(Entry.timestamp.desc())
    return object_list('index.html', query, check_bounds=False)


@app.route('/tag/<slug>/')
def tag_search(slug):
    query = Entry.select().where(Entry.published).where(Entry.tags.contains(slug))
    return object_list('index.html', query, check_bounds=False)


@app.route('/<slug>/', methods=['GET', 'POST'])
def detail(slug):
    if session.get('logged_in'):
        query = Entry.select()
    else:
        query = Entry.public()
    entry = get_object_or_404(query, Entry.slug == slug)
    if request.method == 'POST':
        if request.form.get('name') and request.form.get('content'):
            comment = Comment()
            comment.entry = entry
            comment.name = request.form['name']
            comment.email = request.form['email']
            comment.content = request.form['content']
            comment.save()
        else:
            flash('Must provide content and name', 'danger')
    comments = Comment.select().where(Comment.entry==entry)
    entry.tags = entry.tags.split(',')
    number_of_kudos = Kudos.select().where(Kudos.entry==entry).count()
    return render_template('detail.html', entry=entry, comments=comments, number_of_kudos=number_of_kudos)


@app.route('/contact/', methods=['GET', 'POST'])
def contact():
    if request.method == 'POST':
        if request.form.get('email') and request.form.get('content'):
            if recaptcha.verify():
                sender = request.form['email']
                receiver = ADMIN_EMAIL
                message = request.form['content']
                server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
                server.ehlo()
                server.starttls()
                server.ehlo()
                server.login(ADMIN_SMTP_EMAIL, ADMIN_SMTP_EMAIL_PASSWORD)
                server.sendmail(sender, receiver, message)
                flash("Sent the massage correctly!", 'success')
            else:
                flash("Wrong captcha, try again!")
        else:
            flash("Failed to send the massage!", 'danger')
    return render_template('contact.html')


@app.route('/kudos/<slug>/', methods=['POST'])
def kudos(slug):
    entry = Entry.select().where(Entry.id == slug).first()
    ip = request.headers.get('X-Forwarded-For', request.remote_addr)  # not sure if this is correct solution, will check
    print(ip, file=sys.stderr)
    if Kudos.select().where(Kudos.entry == slug).where(Kudos.ip==ip).count() == 0:
        kudo = Kudos()
        kudo.entry = entry
        kudo.ip = ip
        kudo.save()
    return redirect(url_for('detail', slug=entry.slug))

@app.route('/latestfeed.atom')
def recent_feed():
    feed = AtomFeed(
        'Latest Blog Posts',
        feed_url=request.url,
         url=request.url_root,
         author=request.url_root
     )
    entries = Entry.select().where(Entry.published == True).order_by(Entry.timestamp.desc()).limit(15)
    for entry in entries:
        feed.add(
            entry.title,
            entry.html_content,
            content_type='html',
            url=urljoin(request.url_root, url_for("detail", slug=entry.slug) ),
            updated=entry.timestamp,
            published=entry.timestamp
        )
    return feed.get_response()


@app.template_filter('clean_querystring')
def clean_querystring(request_args, *keys_to_remove, **new_values):
    querystring = dict((key, value) for key, value in request_args.items())
    for key in keys_to_remove:
        querystring.pop(key, None)
    querystring.update(new_values)
    return urllib.parse.urlencode(querystring)


@app.errorhandler(404)
def not_found(exc):
    return Response('<h3>Not found</h3>'), 404


def main():
    database.create_tables([Entry, FTSEntry, Comment, Kudos], safe=True)
    app.run(debug=True)


if __name__ == '__main__':
    main()
